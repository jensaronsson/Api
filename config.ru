# This file is used by Rack-based servers to start the application.

run Rack::File.new("public/assets")

require ::File.expand_path('../config/environment',  __FILE__)
run Rails.application
