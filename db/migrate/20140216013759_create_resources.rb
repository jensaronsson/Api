class CreateResources < ActiveRecord::Migration
  def change
    create_table :resources do |t|
      t.references :resourcetype, index: true
      t.references :user, index: true
      t.references :licence, index: true

      t.timestamps
    end
  end
end
