class CreateResourcetype < ActiveRecord::Migration
  def change
    create_table :resourcetypes do |t|
        t.string :resource_type
        t.timestamps
    end
  end
end
