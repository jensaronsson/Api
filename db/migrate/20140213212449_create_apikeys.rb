class CreateApikeys < ActiveRecord::Migration
  def change
    create_table :apikeys do |t|
      t.string :email
      t.string :apikey
      t.string :update_token
      t.string :token_expires
      t.timestamps
    end
  end
end
