class AddTitleAndDescriptionToResource < ActiveRecord::Migration
  def change
    add_column :resources, :title, :string
    add_column :resources, :description, :string
  end
end
