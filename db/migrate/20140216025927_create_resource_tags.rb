class CreateResourceTags < ActiveRecord::Migration
  def change
    create_table :resource_tags do |t|
      t.references :resource, index: true
      t.references :tag, index: true

      t.timestamps
    end
  end
end
