# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
#   Delete db before seed
# ActiveRecord::Base.establish_connection
# ActiveRecord::Base.connection.tables.each do |table|

#   # SQLite
#     ActiveRecord::Base.connection.execute("DELETE FROM #{table}") unless table == "schema_migrations"
#     ActiveRecord::Base.connection.execute("DELETE FROM sqlite_sequence where name='#{table}'")


# end
# ActiveRecord::Base.connection.execute("VACUUM")
# => Seed db
Admins.create(email: 'admin@admin.se', password: 'demo', password_confirmation: 'demo')

Api::Licence.create(licence_type: 'mit')
Api::Licence.create(licence_type: 'gnu')
Api::Resourcetype.create(resource_type: 'picture')
Api::Resourcetype.create(resource_type: 'movie')
Api::Resourcetype.create(resource_type: 'article')
Api::Resourcetype.create(resource_type: 'repo')
Api::User.create(firstname: 'Kalle', lastname: 'Svensson', email: 'kalle.svensson@gmail.com', password: 'demo', password_confirmation: 'demo')
Api::User.create(firstname: 'Ellen', lastname: 'Nu', email: 'lnu@lnu.se', password: 'lnu', password_confirmation: 'lnu')
Api::Resource.create(resourcetype_id: 1, user_id: 1,
                     licence_id: 1,
                     url: 'http://culttt.com/wp-content/uploads/2013/04/Getting-started-with-Laravel-4.jpg',
                     title: 'Bild på Laravel 4',
                     description: 'Ramverk i php som är superhett just nu, snart det mest nedladdade ramverket i php någonsin.')
Api::Resource.create(resourcetype_id: 3, user_id: 2,
                     licence_id: 1,
                     url: 'https://github.com/nesquena/rabl',
                     title: 'Rabl Ruby',
                     description: 'Rabl är bra.')
Api::Resource.create(resourcetype_id: 3, user_id: 2,
                     licence_id: 1,
                     url: 'https://coursepress.lnu.se/kurs/webbramverk/webb-api-back-end/',
                     title: 'Webb api back end',
                     description: 'Läsvärd artikel!')
Api::Resource.create(resourcetype_id: 4, user_id: 1,
                     licence_id: 1,
                     url: 'https://github.com/fideloper/Vaprobash',
                     title: 'Vaprobash script for vagrant',
                     description: 'Färdigt script för snabb provision av vagrant boxar.')
Api::Tag.create(tag: 'php')
Api::Tag.create(tag: 'programmering')
Api::Tag.create(tag: 'ruby')
Api::Tag.create(tag: 'ramverk')
Api::Tag.create(tag: 'vagrant')
Api::Tag.create(tag: 'vm')
Api::ResourceTag.create(resource_id: 1, tag_id: 1)
Api::ResourceTag.create(resource_id: 1, tag_id: 4)
Api::ResourceTag.create(resource_id: 1, tag_id: 2)
Api::ResourceTag.create(resource_id: 2, tag_id: 3)
Api::ResourceTag.create(resource_id: 2, tag_id: 2)
Api::ResourceTag.create(resource_id: 2, tag_id: 4)

Api::ResourceTag.create(resource_id: 3, tag_id: 2)
Api::ResourceTag.create(resource_id: 4, tag_id: 5)
Api::ResourceTag.create(resource_id: 4, tag_id: 6)
