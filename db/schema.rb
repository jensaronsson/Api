# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140318164038) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: true do |t|
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "apikeys", force: true do |t|
    t.string   "email"
    t.string   "apikey"
    t.string   "update_token"
    t.string   "token_expires"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "licences", force: true do |t|
    t.string   "licence_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "resource_tags", force: true do |t|
    t.integer  "resource_id"
    t.integer  "tag_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "resource_tags", ["resource_id"], name: "index_resource_tags_on_resource_id", using: :btree
  add_index "resource_tags", ["tag_id"], name: "index_resource_tags_on_tag_id", using: :btree

  create_table "resources", force: true do |t|
    t.integer  "resourcetype_id"
    t.integer  "user_id"
    t.integer  "licence_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "url"
    t.string   "title"
    t.string   "description"
  end

  add_index "resources", ["licence_id"], name: "index_resources_on_licence_id", using: :btree
  add_index "resources", ["resourcetype_id"], name: "index_resources_on_resourcetype_id", using: :btree
  add_index "resources", ["user_id"], name: "index_resources_on_user_id", using: :btree

  create_table "resourcetypes", force: true do |t|
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sessions", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tags", force: true do |t|
    t.string   "tag"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
  end

end
