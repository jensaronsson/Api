# Api Docs


## Authentication

To interact with this Api you need to obtain an api-key. You can register with your email and we till serve you with a key.
You can only have one api key per email. Should you lose your key you can reset it by register with your email again.

[Get api-key](http://example.se/register)


## Authenticate

Every request needs to be authenticated with a token.

E.g

    GET:    http://api.se/v1/resources
    Authorization: Token token=YOUR_API_TOKEN

Non authenticated request will be rejected with

        HTTP Token: Access denied.



## Action Methods

## Parameters

### Filters


    eg. http://api.se/v1/resources/?filter=user.firstname&value=john

returns every resource who has a user with firstname of john.
Its a non greedy match so users with a name of "eljohn" or "johns" will be matched.

Greedy match will be supported in further version.

Available filters

    *   user.firstname
    *   user.lastname
    *   user.email
    *
    *   resource.type
    *   licence.type
    *
    *   tags.tag


### Limit & Offset

    eg. http://api.se/v1/resources/?limit=10&offset=2

Returns 10 resources starting from the second resource.

Default limit is 20.




## Get all resources

  `GET: http://api.se/v1/resources`

Returns all resources. Default limit of 20 resources.



Example Request




            [
    {
        "resource": {
            "id": 1,
            "url": "http://culttt.com/wp-content/uploads/2013/04/Getting-started-with-Laravel-4.jpg",
            "updated_at": "2014-02-20T20:33:41.185Z",
            "created_at": "2014-02-20T20:33:41.185Z",
            "resourcetype": "Picture",
            "tags": [
                {
                    "tag": "php"
                },
                {
                    "tag": "ramverk"
                },
                {
                    "tag": "programmering"
                }
            ],
            "licence": "mit",
            "links": [
                {
                    "rel": "self",
                    "href": "http://rails.dev/v1/resources/1"
                }
            ],
            "user": {
                "id": 1,
                "firstname": "Jens",
                "lastname": "Aronsson",
                "email": "jensarons@gmail.com",
                "links": [
                    {
                        "href": "http://rails.dev/v1/users/1",
                        "rel": "profile"
                    },
                    {
                        "href": "http://rails.dev/v1/users/1/resources",
                        "rel": "profile.resources"
                    },
                    {
                        "href": "http://www.gravatar.com/avatar/dbb23811c7a52b7c251b07b5d3bc6e37",
                        "rel": "gravatar"
                    }
                ]
            }
        }
    },


## Create new resource


POST: http://api.se/v1/resources

Creates a new resource.

Request payload with json

        {
          "resourcetype_id": 1,
          "user_id": 1,
          "licence_id":1,
          "url": "http://feber.se",
          "tags": ["test", "test2"]

        }


Returns

{
    "status": {
        "status_code": 201,
        "message": "Resource was created successfully"
    }
}


## Delete resource

DELETE:  http://api.se/v1/resources/1

Deletes resource with the specific id

Returns

    {
        "status": {
            "status_code": 201,
            "message": "Resource was deleted successfully"
        }
    }



## Update resource

PUT:  http://api.se/v1/resources/1

Updates resource with specific id with posted changes

Request payload

        {
             "resourcetype_id": 1,
             "user_id": 2, <- changed from 1 to 2
             "licence_id":1,
             "url": "feber.se",
                         "tags": ["tag1", "tag2"]

        }

Returns

        {
           "status": {
               "status_code": 201,
               "message": "Resource was updated successfully"
           }
        }


# Users

You can fetch specific users and see all resources posted by that user.


## Get user

    GET:  http://api.se/v1/users/1

 Returns user with the id of 1

     {
        "user": {
            "id": 1,
            "firstname": "john",
            "lastname": "doe",
            "email": "johndoe@gmail.com",
            "links": [
                {
                    "rel": "self",
                    "href": "http://api.se/v1/users/1"
                }
            ]
        }
    }



## Get users resources

    GET:  http://api.se/v1/users/1/resources

Returns resources associated with the user with an id of 1

    [
        {
            "resource": {
                "id": 4,
                "url": "https://github.com/fideloper/Vaprobash",
                "updated_at": "2014-02-20T20:33:41.242Z",
                "created_at": "2014-02-20T20:33:41.242Z",
                "resourcetype": "Repo",
                "tags": [
                    {
                        "tag": "vagrant"
                    },
                    {
                        "tag": "vM"
                    }
                ],
                "licence": "mit",
                "links": [
                    {
                        "rel": "self",
                        "href": "http://api.se/v1/resources/4"
                    }
                ],
                "user": {
                    "id": 1,
                    "firstname": "Jens",
                    "lastname": "Aronsson",
                    "email": "jensarons@gmail.com",
                    "links": [
                        {
                            "href": "http://api.se/v1/users/1",
                            "rel": "profile"
                        },
                        {
                            "href": "http://api.se/v1/users/1/resources",
                            "rel": "profile.resources"
                        },
                        {
                            "href": "http://www.gravatar.com/avatar/dbb23811c7a52b7c251b07b5d3bc6e37",
                            "rel": "gravatar"
                        }
                    ]
                }
            }
        }
    ]
