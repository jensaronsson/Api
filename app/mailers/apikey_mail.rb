class ApikeyMail < ActionMailer::Base
  default from: 'from@example.com'

  def key_generated(user)
    @user = user
    mail subject: "Your api key", to: @user["email"]
  end

  def confirm_update(user)
    @user = user
    mail subject: "Confirm update of api key", to: @user["email"]

  end

end
