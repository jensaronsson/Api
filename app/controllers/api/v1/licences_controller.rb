module Api
  module V1

        class LicencesController < ApiBaseController

            def index
                @licences = Licence.all()
            end
        end

  end
end
