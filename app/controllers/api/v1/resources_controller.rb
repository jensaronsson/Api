module Api
  module V1
    class ResourcesController < ApiBaseController
      before_action :auth_access_token, only: [:edit, :update, :destroy, :create]
      before_action :set_resource, only: [:show, :edit, :update, :destroy]
      before_action :if_filter, only: [:index]

      # GET /resources
      # GET /resources.json
      def index
        # Loaded resources with if_filter callback
        @resources
        # if user want resources on specific user


      end




      # GET /resources/1
      # GET /resources/1.json
      def show
        @resource
      end

      # POST /resources
      # POST /resources.json
      def create
        @resource = Resource.create!(resource_params)
        puts resource_params
        has_tags


        @resource.save
        set_status 201, "Resource was created successfully"


      end

      # PATCH/PUT /resources/1
      # PATCH/PUT /resources/1.json
      def update
        puts "params"
        @resource.update(resource_params);
        has_tags
        if @resource.valid?
          @resource.save
          set_status 200, "Resource was updated successfully"
        else
          set_status 404, @resource.errors.full_messages
        end
      end

      # DELETE /resources/1
      # DELETE /resources/1.json
      def destroy
        @resource.destroy
        set_status 200, "Resource was deleted successfully"
      end

      private

      def if_filter
        @offset = params[:offset] || nil
        @limit  = params[:limit]  || 10
        filters = {"user.lastname" => "users.lastname", "user.firstname" => "users.firstname", "user.email" => "users.email",
                    "tags.tag" => "tags.tag", "resource.type" => "resourcetypes.resource_type", "licence.type" => "licences.licence_type"}

        if params.has_key?(:filter) && params.has_key?(:value)
          @resources = Resource.includes(:resourcetype, :user, :licence, :tags)
                            .where("#{filters[params[:filter]]} LIKE ?", "%#{params[:value].downcase}%" )
                            .offset(params[:offset]).limit(params[:limit])
          if @resources.blank?
            raise ActiveRecord::RecordNotFound
          end
        else
          @resources = Resource.includes(:resourcetype, :user, :licence, :tags)
            .order("created_at DESC")
            .limit(@limit)
            .offset(@offset)
            pagination()

        end
      end


      def pagination

        base = url_for :controller => 'resources', :action => 'index'
        count = Resource.distinct.count(:id)

        offset = @offset.to_i
        limit  = @limit.to_i
        nextDiff = (offset + limit)
        prevDiff = (offset - limit)
        totCount = (count / limit) + 1
        pages = (count - offset) / limit
        page = (totCount - pages)


        @pagination = Array.new()
        @pagination.push({pages: pages, page: page,
                          total_pages: totCount,
                          results: @resources.count,
                          offset: offset,
                          limit: limit  })


        if limit <= @resources.count
          @pagination.push({:href => base + "?offset=#{nextDiff}&limit=#{@limit}", :rel => 'next', :page => page+1})
        end
        if prevDiff >= 0
          @pagination.push({:href => base + "?offset=#{prevDiff || 0}&limit=#{@limit}", :rel => 'previous', :page => page-1 })
        end

      end
      def has_tags
        if params.has_key?(:tags) && params[:tags]
          params[:tags].each do |key, val|
              @resource.tags.find_or_initialize_by(:tag => key.downcase)
            end

        end
      end

      # Use callbacks to share common setup or constraints between actions.
      def set_resource
        @resource = Resource.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def resource_params
        @tags = params[:tags]
        params.require(:resource).permit(:resourcetype_id, :licence_id,
          :user_id, :url, :title, :description, :tags, :id)
      end
    end

  end
end
