module Api
  module V1

    class ErrorController < ApiBaseController

        def notfound
           raise ActionController::RoutingError.new(params[:path])
        end
    end
  end
end
