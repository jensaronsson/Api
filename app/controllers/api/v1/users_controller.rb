module Api
  module V1
    class UsersController < ApiBaseController
      before_action :set_user, only: [:show, :edit, :update, :destroy]

      def index
       @users = User.all
      end

      def show
        @user
      end

      private


      # Use callbacks to share common setup or constraints between actions.
      def set_user
        @user = User.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        set_status 404, "User not found"

      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def user_params
        params.require(:user).permit(:firstname, :lastname, :email)
      end
    end
  end
end
