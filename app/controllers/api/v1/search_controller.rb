module Api
  module V1
    class SearchController < ApiBaseController


      def search
        @query = params[:q]
        @offset = params[:offset] || nil
        @limit  = params[:limit]  || 10
        @count = Resource.search(@query).distinct.count(:id)
        @resources = Resource.search(@query).offset(@offset).limit(@limit)

        pagination()
        render "api/v1/resources/index"
      end

      def pagination
        base = url_for :controller => 'search', :action => 'search'

        offset = @offset.to_i
        limit  = @limit.to_i
        nextDiff = (offset + limit)
        prevDiff = (offset - limit)
        totCount = (@count / limit) + 1
        pages = (@count - offset) / limit
        page = (totCount - pages)


        @pagination = Array.new()
        @pagination.push({pages: pages, page: page,
                          total_pages: totCount,
                          results: @resources.count,
                          offset: offset,
                          limit: limit  })


        if limit <= @resources.count
          @pagination.push({:href => base + "?q=#{@query}&offset=#{nextDiff}&limit=#{@limit}", :rel => 'next', :page => page+1})
        end
        if prevDiff >= 0
          @pagination.push({:href => base + "?q=#{@query}&offset=#{prevDiff || 0}&limit=#{@limit}", :rel => 'previous', :page => page-1 })
        end

      end
    end
  end
end
