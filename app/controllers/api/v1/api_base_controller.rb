module Api
  module V1

    class ApiBaseController < ::ActionController::Base
      # Prevent CSRF attacks by raising an exception.
      # For APIs, you may want to use :null_session instead.
      # protect_from_forgery with: :null_session
      rescue_from Exception, :with => :handle_exception
      before_action :auth_api_token
      skip_before_filter :auth_api_token
      before_filter :cors_preflight_check
      after_filter :cors_set_access_control_headers


      def options
        render :text => '', :content_type => 'text/plain'
      end

      # For all responses in this controller, return the CORS access control headers.
      def cors_set_access_control_headers
        headers['Access-Control-Allow-Origin'] = '*'
        headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS, PUT, DELETE'
        headers['Access-Control-Max-Age'] = "1728000"
      end

      # If this is a preflight OPTIONS request, then short-circuit the
      # request, return only the necessary headers and return an empty
      # text/plain.

      def cors_preflight_check
        headers['Access-Control-Allow-Origin'] = '*'
        headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS, PUT, DELETE'
        headers['Access-Control-Allow-Headers'] = '*, x-requested-with, Content-Type,
        If-Modified-Since, If-None-Match, Auth-Token, Authorization, X-Requested-With'
        headers['Access-Control-Max-Age'] = '1728000'
      end



      private

      def handle_exception exception
        puts exception.inspect
        case exception
        when ActiveRecord::RecordNotFound
          set_status 404, "Resource not found"
        when ActiveRecord::RecordInvalid
          set_status 422, exception.message
        when ActionController::RoutingError
          set_status 404, "oops, this path is nothing yet!"
        when JWT::DecodeError
          set_status 401, "Token is not valid. Renew it."
        when NameError
          set_status 500, exception.message
        else
          set_status 500, "Something wrong happend"
        end

      end


      def set_status code, message
        puts "message"
        puts message
        response.status = code
        @status_message = message
        render "api/v1/shared/status"
      end

      def auth_api_token
        authenticate_or_request_with_http_token do |token, options|
          ::Apikey.exists?(apikey: token)
        end
      end

      def auth_access_token
        token = getAuthToken
        if !validToken(token)
            set_status 401, "The token has expired. Renew it"
        end
      end

      def decodeJWT token
        return ::JWT.decode(token, "venga")
      end

      def getAuthToken
        return request.headers['Auth-Token']
      end

      def validToken token
        if(token.nil?)
          return set_status 400, "Token is missing"
        end
        token = decodeJWT(token)
        return token["exp"] > Time.now
      end



    end
  end
end
