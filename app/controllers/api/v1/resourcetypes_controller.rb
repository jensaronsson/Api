module Api
  module V1

    class ResourcetypesController < ApiBaseController

        def index
            @resourcetypes = Resourcetype.all()
        end



      # Use callbacks to share common setup or constraints between actions.
      # def set_resourcetypes
      #   @resourcetypes = Resource.find(params[:id])
      # end

      # Never trust parameters from the scary internet, only allow the white list through.
      def resourcetypes_params
        params.require(:resourcetypes).permit(:resource_type)
      end

    end

  end
end
