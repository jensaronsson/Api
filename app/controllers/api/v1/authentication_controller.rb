module Api
    module V1

        class AuthenticationController < ApiBaseController


            def authenticate_user
                @user = User.find_by_email(params[:email])
                if @user && @user.authenticate(params[:password])
                    get_jwt_token
                    return render "api/v1/authentication/index"
                else
                    set_status(403, "Username/Password are wrong")
                end
            end

            def isLoggedIn
                @token = getAuthToken
                @isLoggedIn = validToken(@token)
                unless(@isLoggedIn.nil?)
                    @decodedToken = decodeJWT(@token)
                    @decodedToken["active"] = @isLoggedIn
                    @decodedToken.delete("exp")
                    render "api/v1/authentication/status"
                end
            end


            def get_jwt_token
                token_expires = Time.now + 60*60*24
                user = {
                    exp: token_expires,
                    firstname: @user[:firstname],
                    lastname: @user[:lastname],
                    email: @user[:email],
                    id: @user[:id]
                }
                @token = ::JWT.encode(user, "venga")  # todo, change secret in production
            end

        end
    end
end
