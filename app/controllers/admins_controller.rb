class AdminsController < ApplicationController
    before_action  :require_login

    def index
        @apikeys = Apikey.all
    end

    def logged_in?
        session[:admin_id].present?
    end

    private

    def require_login
        unless logged_in?
          flash[:error] = "You must be logged in to access this section"
          redirect_to "/login" # halts request cycle
        end
    end

end
