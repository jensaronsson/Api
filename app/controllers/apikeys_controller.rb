class ApikeysController < ApplicationController




    def new
        render :index
    end

    def create

        @email   = params[:email]

        if(@email.empty?)
            return redirect_to root_url, flash: { apikey: { msg: "You must enter a email address"} }
        end
        @user    = Apikey.find_by(email: @email)

        # If useremail doesnt exists -> create a new and email the key.
        if @user == nil

            @user = Apikey.create(email: @email)

            ApikeyMail.key_generated(@user).deliver
            if request.xhr?
                render json: { :apikey => @user.apikey, :message => "This is your api-key, the has also been sent by email." }
            else
                redirect_to root_url, flash: { apikey: { msg: "This is your api-key", key: @user.apikey} }
            end

        else # if useremail exists send a confirm mail before updating.

            @user.update_token = SecureRandom.hex
            @user.token_expires = Time.now + 15*60
            @user.save

            ApikeyMail.confirm_update(@user).deliver

            if request.xhr?
                render :json => { :message =>"An email has been sent for update confirmation." }
            else
                redirect_to root_url, flash: { apikey: { msg: "An email has been sent for update confirmation.", email: @email} }
            end
        end

    end

    def updatetoken

        @token = params[:updatetoken]
        @user = Apikey.find_by(update_token: @token)

        if @user && @user.token_expires > Time.now
            @user.update_token = nil
            @user.token_expires = nil
            @user.save
            redirect_to root_url, flash: { apikey: { msg: "Your key has been updated, check your email", email: @user.email} }
        else
            redirect_to root_url, flash: { apikey: { msg: "Url has expired!", email: nil } }
        end

    end

    def destroy
        Apikey.find_by_id(params[:id]).destroy
        redirect_to "/admin/apikeys"

    end



end
