class SessionsController < ApplicationController


    # try to log in user
    def create
        user = Admins.find_by_email(params[:email])

        if user && user.authenticate(params[:password])
            session[:admin_id] = user.id
            redirect_to '/admin/apikeys'
        else
            flash.now[:notice] = "Wrong username/password"
            render :login
        end

    end

    # Get userlogin form
    def new

        render :login
    end

    def destroy
        session[:admin_id] = nil
        redirect_to root_url :notice => "Logged out"
    end



end
