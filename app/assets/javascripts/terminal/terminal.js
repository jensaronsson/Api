(function ($) {
'use strict';

_.templateSettings = {
    interpolate: /\{\{\=(.+?)\}\}/g,
    evaluate: /\{\{(.+?)\}\}/g
};




t.Collections.Output = Backbone.Collection.extend({
    model: t.Models.Output
});


t.Views.Output = Backbone.View.extend({

    className: "large-12 columns terminal-message",

    template: _.template( $('#terminal-output').html() ),

    initialize: function() {
        this.render();
    },

    render: function() {
        console.log(this.model.toJSON());
        this.$el.html( this.template( this.model.toJSON() ) );
        return this;
    }
});

// t.Views.Outputs = Backbone.View.extend({
//     el: '.terminal-messages',

//     initialize: function() {
//         this.render();
//     },

//     render: function() {
//         this.$el.html("hej");
//     }
// });

// t.Views.Terminal = Backbone.View.extend({
//     el: '.terminal',

//     events : {
//         'keypress #terminal ' : 'createOutput'
//     },

//     initialize: function() {
//         this.$messages = this.$('.terminal-messages');
//         this.$input = this.$('#terminal');
//         this.$input.focus();

//         this.listenTo(outputs, 'add', this.addOne);
//         this.listenTo(outputs, 'reset', this.clearTerminal);


//     },
//     createOutput : function(e) {
//         if (e.keyCode == 13) {
//             e.preventDefault();
//             outputs.add( { output: this.$input.html() } )
//             this.$input.html("");
//         }
//     },

//     addOne : function(output) {
//         var view = new t.Views.Output( { model : output } );
//         this.$messages.append(view.render().el);
//     },

//     clearTerminal : function(collection) {
//         this.$messages.empty();
//     }
// });


// var outputs = new t.Collections.Output();
// new t.Views.Terminal();






})(jQuery);


