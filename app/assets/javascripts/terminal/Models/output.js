define([
    'backbone'
], function(Backbone) {
    'use strict';

    var Output = Backbone.Model.extend({
        initialize : function() {
            this.checkActions();
        },

        checkActions : function() {

            var that = this;
            var actions = {
                "help" : function() {
                    var helpInfo = "Available commands.<br>";
                    for(var prop in actions) {
                       helpInfo += prop + "<br>";
                    }
                    return helpInfo;

                },
                "register" : function() {
                    // register [email]
                    return "Register";
                },
                "auth" : function() {
                    // auth [email] [password]
                    // auth [email] return password prompt
                    // auth reset [email]

                    return "Authenticated";
                },
                "clear" : function() {
                    console.log(that);
                    that.collection.reset();
                }

            };

            if (actions.hasOwnProperty(this.attributes.output)) {
                this.attributes.output = actions[this.attributes.output]();
            }

        }

    });
    return Output;
});

