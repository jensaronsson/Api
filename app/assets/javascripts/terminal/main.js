'use strict';

require.config({
    shim: {
        underscore:  {
            exports: '_'
        },
        backbone: {
            deps: [
                'jquery',
                'underscore'
            ],
            exports: 'backbone'
        }
    },
    paths: {
        jquery : '../jquery',
        backbone : '../backbone',
        underscore : '../underscore',
        text: '../text'
    }
});

require([
    'Views/output',
    'Models/output',
    'text!Templates/output.html'
], function (outputView, outputModel, tpl) {
    /*jshint nonew:false*/
    // Initialize routing and start Backbone.history()
    console.log(tpl);
    new outputView({model: new outputModel()})
});