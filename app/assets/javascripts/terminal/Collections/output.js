define([
    'backbone',
    'Models/output'
], function(Backbone, modelOutput ) {

    var Output = Backbone.Collection.extend({
        model: modelOutput
    });
    return Output;
});