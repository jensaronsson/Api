define([
    'backbone'
], function(Backbone) {
    'use strict';
    console.log(Backbone);
    var Output = Backbone.View.extend({

        className: "large-12 columns terminal-message",

        initialize: function() {
            this.render();
        },

        render: function() {
            console.log(this.model.toJSON());
            this.$el.html( this.template( this.model.toJSON() ) );
            return this;
        }
    });
    return Output;
})