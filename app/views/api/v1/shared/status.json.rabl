object false
child :status do
   node(:status_code) { response.status }
   node(:message) { @status_message }
    if(@resource && @resource[:id])
        node(:resource) { @resource[:id] }
    end
end
