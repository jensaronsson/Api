object @user
attributes :id, :firstname, :lastname, :email

node :links do |m|
    [{ :rel => "self", :href => api_v1_user_url(m) }]
end