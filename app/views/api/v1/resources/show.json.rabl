node(:status_code) {response.code}
object @resource
attributes :id, :url, :updated_at, :created_at, :resourcetype, :tags, :title, :description
child :licence do |licence|
  attribute :id, :licence_type
end


child :resourcetype do |resourcetype|
  attribute :id, :resource_type
end

child :user do |user|
    attribute :id, :firstname, :lastname, :email

    node :links do |u|
        [
            {:href => api_v1_user_url(user), :rel => "profile"  },
            {:href => api_v1_user_resources_url(user), :rel => "profile.resources"  },
            {:href => "http://www.gravatar.com/avatar/" + u.gravatarUrl, :rel => "gravatar"  },
        ]
    end
end

child :tags do |m|
    attribute :tag
end


node :links do |m|
    [{ :rel => "self", :href => api_v1_resource_url(m) }]
end


