node(:status_code) {response.code}
node(:self) {request.original_url}

node :pagination do
    @pagination
end

node :resources do
    @resources.map {|r| partial "api/v1/resources/show", object: r}
end
