object false
node(:status_code) {response.code}
node(:self) {request.original_url}
node(:token) { @token }
child @user => :user do
attributes :firstname, :lastname, :id, :email
node(:gravatar) {"http://www.gravatar.com/avatar/" + @user.gravatarUrl}
end