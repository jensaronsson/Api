class Apikey < ActiveRecord::Base
    before_create :generate_api_key
    before_update :generate_api_key



    def generate_api_key
        begin
            self.apikey = SecureRandom.urlsafe_base64(54)
        end while Apikey.exists?(apikey: apikey)
    end
end
