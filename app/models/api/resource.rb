module Api
  class Resource < ActiveRecord::Base
    belongs_to :resourcetype
    belongs_to :user
    belongs_to :licence
    has_many :resource_tag
    has_many :tags, through: :resource_tag

    validates :url, presence: true
    validates :title, presence: true
    validates :description, presence: true

    validates_format_of :url,
        :with => /^(http|https):\/\/[a-z0-9]+([-.]{1}[a-z0-9]+)* .[a-z]{2,5}(([0-9]{1,5})?\/.*)?$/ix,
         :message => 'is in wrong format. Correct url e.g: http://www.feber.se', :multiline => true


    validates_each :user_id, :resourcetype_id, :licence_id do |record, attr, value|

      cString = attr.to_s.sub!(/_id/,'')
      class_string = cString.camelize
      class_string = 'Api::' + class_string

      result = class_string.constantize.find_by(:id => value)

      if result.blank?
        record.errors.add(attr, "with id: #{value}, doesnt exist")
      end


    end

    def self.search query
      q = "%#{query}%"
      where("title ilike ? or description ilike ?", q, q)
    end






  end
end
