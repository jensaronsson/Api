module Api
    class User < ActiveRecord::Base
        has_secure_password
        def gravatarUrl
            Digest::MD5.hexdigest(self.email)
        end
    end
end