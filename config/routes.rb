Vagrant::Application.routes.draw do



  # Routes concerning api token registrations
  root  'apikeys#index'
  get   '/login' => 'sessions#new'
  get   '/logout' => 'sessions#destroy'


  resources :sessions, :apikeys, :login

  get   '/register' => 'apikeys#new'
  post  '/' => 'apikeys#create'
  get   'confirmkey/:updatetoken' => 'apikeys#updatetoken'

  get   '/ask' => 'terminal#ask'


  scope 'admin' do
    get '/apikeys' => 'admins#index'
  end



  # Routes to main api.
  # constraints subdomain: 'api' do
    scope module: :api, as: :api do
      namespace :v1, defaults: {format: 'json'} do
        post '/authenticate' => 'authentication#authenticate_user'
        get '/isloggedin' => 'authentication#isLoggedIn'
        get '/resources/search' => 'search#search'
        resources :resourcetypes
        resources :licences
        resources :resources
        resources :users do
          resources :resources
        end
        match "*", to: "error#notfound", via: [:get, :post, :delete, :put, :patch]
        match '*any' => 'api_base#options', :constraints => {:method => 'OPTIONS'}, via: [:options]

      end
    end
  # end







  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
