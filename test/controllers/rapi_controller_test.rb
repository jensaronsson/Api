require 'test_helper'

class RapiControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get generatekey" do
    get :generatekey
    assert_response :success
  end

end
